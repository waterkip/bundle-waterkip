#!/usr/bin/perl
use warnings;
use strict;

# PODNAME: jwt-decrypt
# ABSTRACT: Decrypt JWT tokens

require Bundle::WATERKIP::CLI::JWT;
Bundle::WATERKIP::CLI::JWT->run()

__END__

=head1 SYNOPSIS

get-azure-token.pl --help [ OPTIONS ]

=head1 OPTIONS

=over

=item * --help (this help)

=back

