#!/usr/bin/perl
use warnings;
use strict;

# PODNAME: get-azure-token.pl
# ABSTRACT: Get JWT tokens from Azure

require Bundle::WATERKIP::CLI::Azure;
Bundle::WATERKIP::CLI::Azure->run()

__END__

=head1 SYNOPSIS

get-azure-token.pl --help [ OPTIONS ]

=head1 OPTIONS

=over

=item * --help (this help)

=back

