#!/usr/bin/perl
use warnings;
use strict;

# PODNAME: tdiff.pl
# ABSTRACT: A small cli script to add time intervals to a date

use Getopt::Long;
use DateTime;
use DateTime::Format::ISO8601;
use Pod::Usage;

my %options = (
    tz => 'local'
);

GetOptions(
    \%options, qw(
        hours|hrs=i
        minutes|mins=i
        days=i
        weeks|wks=i
        years|yrs=i
        date=s
        time=s
        tz=i
        help|h
        )
);

if ($options{help}) {
    pod2usage({ verbose => 1, exitval => 0 });
}
my $date = $options{date}
    ? DateTime::Format::ISO8601->parse_datetime($options{date})
    : DateTime->now();

$date->set_time_zone($options{tz});

my $now = $date->clone();

foreach (qw(years weeks days minutes hours)) {
    $date->add($_ => $options{$_}) if exists $options{$_};
}

printf("%s => %s$/", $now, $date);

__END__

=head1 SYNOPSIS

tdiff OPTIONS

=head1 OPTIONS

=over

=item date

A date, defaults to now

=item minutes|mins

Minutes to add

=item hours|hrs

Hours to add

=item days

Days to add

=item weeks

Weeks to add

=item years

Years to add

=item help

This help
