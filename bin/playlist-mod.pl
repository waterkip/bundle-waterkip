#!/usr/bin/env perl
#
#PODNAME: playlist-mod.pl
#
#ABSTRACT: Modify m3u playlists

use warnings;
use strict;
use Data::Dumper;
use List::Util qw(any);
use Text::CSV_XS;

use autodie;
use utf8;
use v5.26;

use Getopt::Long;
use YAML::XS qw(LoadFile);
use File::Spec::Functions qw(catfile splitpath);
use List::Util qw(first);

my %options = (
  config => catfile($ENV{HOME}, qw(.config iptv config)),
);

GetOptions(\%options, qw(help man config=s playlist=s out));

if (!-f $options{config} || ! -r $options{config}) {
    die "Unable to open configuration file $options{config}";
}

my $config = LoadFile($options{config});

my $m3u = $options{playlist} // $config->{playlist}{local};
my $output = $options{out} // $config->{playlist}{output};

$m3u =~ s/(?:\$([A-Z0-9_a-z]+))/$ENV{$1}/g;
$output =~ s/(?:\$([A-Z0-9_a-z]+))/$ENV{$1}/g;

open my $fh, '<', $m3u;

my @tracks;

my $start= 0;
my $audio = 0;
my $iptv = 0;

sub _process_line {
  my $line = shift;
  chomp($line);
  $line =~ tr/\r//d;
  return $line;
}

my @ok = ();

my %section;
my %seen;
my @nope;

my $processed =0;

sub country_to_code {
    my $needle = shift;
    return 'INT' unless $needle;

    my $n = $needle =~ s/(?:Movies|Series|VOD)\s*//r;
    return "INT" unless $n;
    $needle = $n;

    my @things = split(/\|?\s+/, $needle);
    if ($things[0] eq '24/7') {
      return 'INT';
    }
    if ($things[0] eq 'USA' || $things[0] eq 'US') {
      return 'USA';
    }

    state %countries = (
      Afghanistan => '',
      Africa => '',
      Albania => '',
      Algeria => '',
      Angola => '',
      Arab => '',
      Arabic => '',
      Argentina => 'ARG',
      Armenia => '',
      Asia => '',
      Australia => '',
      Austria => '',
      Azerbaijan => '',
      Bahrain => '',
      Bambini => '',
      Bangladesh => '',
      Bein => '',
      Belarus => '',
      Belgium => '',
      Benin => '',
      Bolivia => '',
      Bosnia => '',
      Brazil => '',
      Bulgaria => '',
      Burkina => '',
      Cambodia => '',
      Cameroon => '',
      Canada => '',
      Canale => '',
      Carib => '',
      Caribbean => '',
      Chile => 'CHI',
      China => '',
      Colombia => 'COL',
      Congo => '',
      'Costa Rica' => 'CR',
      Croatia => '',
      Cuba => 'CUB',
      Cyprus => '',
      Czech => 'CZE',
      Côte => '',
      Denmark => 'DEN',
      Dominican => '',
      Ecuador => 'ECU',
      Egypt => 'EGY',
      Ethiopia => '',
      'Ex-Yugoslavia' => '',
      Finland => 'FIN',
      France => 'FRA',
      Georgia => '',
      Germany => 'GER',
      Ghana => 'GHA',
      Greece => 'GRE',
      Guatemala => '',
      Guinea => '',
      Honduras => '',
      Hungary => '',
      India => '',
      Indonesia => '',
      Iran => '',
      Iraq => '',
      Ireland => 'IRE',
      Islamic => '',
      Israel => 'ISR',
      Italy => 'ITA',
      Japan => 'JPN',
      Jordan => '',
      Kazakhistan => '',
      Kenya => '',
      Korea => 'KOR',
      Kurdistan => '',
      Kuwait => '',
      Lebanon => '',
      Libya => '',
      Macedonia => '',
      Malaysia => '',
      Mali => '',
      Malta => '',
      Mauritania => '',
      Mexico => 'MEX',
      Mongolia => '',
      Montenegro => '',
      Morocco => '',
      Mozambique => '',
      Nepal => '',
      Netherland => 'NLD',
      Nicaragua => '',
      Nigeria => '',
      Norway => '',
      Oman => '',
      Pakistan => '',
      Palestine => '',
      Panama => '',
      Paraguay => '',
      Pay => '',
      Peru => '',
      Philippines => '',
      Poland => '',
      Portugal => '',
      'Puerto Rico' => 'PUR',
      Qatar => '',
      Romania => '',
      Russia => 'RUS',
      Rwanda => '',
      Saudi => '',
      Senegal => '',
      Serbia => '',
      Slovakia => '',
      Somal => '',
      South => '',
      Spain => 'ESP',
      Sudan => '',
      Suriname => 'SUR',
      Sweden => 'SWE',
      Switzerland => 'SUI',
      Syria => '',
      Taiwan => '',
      Tanzania => '',
      Thailand => '',
      Tunisia => '',
      Turkey => '',
      USA => 'USA',
      Uganda => '',
      Ukraine => 'UKR',
      Uruguay => 'URU',
      Venezuela => 'VEN',
      Vietnam => '',
      Yemen => 'YEM',

      Action => '',
      French => 'FRA',
      Netherlands => 'NLD',
      Reality => '',
      Soap => '',
      Action => '',
      News => '',
      Sports => '',
      EPL => '',
      Religion => '',
      Sports => '',

      Cultura => '',
      NFL => 'USA',

      Turkish => 'TUR',
      'EX-YU' => 'YUG',
      Talk => '',
      Arabic => '',

      XXX => '',
      Drama => '',
      Music => '',
      Horror => '',
      Thriller => '',
      Comedy => '',
      Bollywood => 'IND',
      Western => '',
      'Sci-Fi' => '',
      Classic => '',
      Christmas => '',
      Documentary => '',
      Romance => '',
      War => '',
      Animation => '',
      Mystery => '',
      Crime => '',
      Anime => '',
      Adventure => '',
      Kids => '',
      'TV Movie' => '',
      Fantasy => '',
      Concerts => '',
      'Kung Fu' => '',

      'Indian' => 'IND',
      'UFC' => 'USA',
      'Amazon Prime' => 'INT',
      'United Arab Emirates UAE' => "UAE",
      HBO => 'USA',
      Lifetime => 'USA',
      History => 'USA',
      Netflix => '',
      'Kung Fu' => '',
      Family => '',
      'Other Sports Replays' => '',
      'Sky Sports' => 'ENG',
      'New Zealand Sky' => 'NZE',
      Radio => '',
      'Sports Cricket', 'ENG',
      Entertainment => '',

    );

    if (exists $countries{$needle}) {
      return $countries{$needle} || 'INT';
    }
    if (exists $countries{$things[0]}) {
      return $countries{$needle} || 'INT';
    }

    warn "searching '$needle'", $/;

    state @countries = keys %countries;
    my $found = first { $needle =~ /^$_\b/ } @countries;
    return 'INT' unless defined $found;
    return $countries{$found} || 'INT';
}

while (my $line = $fh->getline()) {

  $line = _process_line($line);

  # This should be the first line of the file
  if ($line =~ /^#EXTM3U$/) {
    $start = 1;
    next;
  }
  next unless $start;

  # Unknown what this does
  next if $line =~ /^#EXT-X-SESSION-DATA:DATA-ID/;
  next if $line !~ /^#EXTINF:/;

  $line =~ s/^#EXTINF:\s*//;

  my %data;
  my @contents = split(/,/, $line);

  my $title = @contents > 1 ? pop @contents : "";
  $title =~ s/^\s+//;

  $line = join(',', @contents);

  #EXTINF:-1,Artist-name - Title-name
  my ($runtime, @misc) = split(/\s+/, $line);

  if (!@misc) {
    # Audio
    my ($artist, $song) = split(/ - /, $title, 2);
    $data{artist} = $artist;
    $data{title} = $song;
    my $uri = _process_line($fh->getline());
    $data{uri} = $uri;
    push(@tracks, \%data);
    next;
  }

  $line = join(' ', @misc);
  while ($line =~ /([[:word:]-]+)=("[^"]*")\s*/g) {
      $data{$1} = substr($2, 1, -1); # remove the ""
  }

  my $uri = _process_line($fh->getline());

  $data{runtime} = $runtime;

  $data{uri} = $uri;

  foreach (qw(group-title tvg-name title)) {
      next unless exists $data{$_};
      $data{$_} =~ s/^\w+: //;
  }

  #if ($data{'tvg-logo'} =~ /_at_/) {
  #    my $uri = $data{'tvg-logo'};
  #    my @uri = split(/\//, $uri);
  #    if ($uri[-1] =~ /_at_/) {
  #        my ($away, $home) = split(/_at_/, $uri[-1]);
  #        $home =~ s/\..+//;
  #        $data{title} = "$away at $home";
  #        $data{'tvg-name'} = $data{title};
  #    }
  #}

  my $group = $data{'group-title'};

  if ($group eq 'Local') {
      $group = 'Movies';
  }
  elsif ($title =~ /Bangbro/i) {
    $group = "VOD Adults";
  }
  elsif ($group eq 'Private (18+) Vods') {
    $group = "VOD Adults";
  }
  elsif ($group eq 'USA Local Channels ( Full List )') {
      $group = 'USA Local - All';
  }
  elsif ($group =~ /^Adults/) {
      $group = "Adults";
  }
  # This is a weird one
  elsif ($group eq 'Adult') {
      $group = 'Movies';
  }
  elsif ($group eq 'USA NFL - Sunday Ticket') {
      $group = 'USA NFL';
  }
  elsif ($group eq 'WWE Boxing') {
      $group = "Sports $group";
  }
  elsif ($group eq 'US| NFL PPV') {
      $group = 'USA NFL';
  }

  if ($title =~ /VOD X/) {
      $group = "VOD Adults";
      $title =~ s/VOD X:?\s*//g;
  }
  if ($title =~ /Adult \|/) {
      $group = "VOD Adults";
      $title =~ s/Adult \|\s*//g;
  }
  if ($title =~ /S[0-9]+E[0-9]+/ && $group !~ /serie/i) {
    $group = "Series";
  }

  # Yemen
  $group = "Yemen" if $group eq 'Yeman';


  $group =~ s/^([[:alnum:]]+)\s*-\s*(\w)/$1 $2/;
  $group =~ s/^(3D)(.*)$/$2 $1/i;
  $group =~ s/^(movie|serie|sport)\b/$1s/i;
  $group =~ s/^(Pakistan)i\b/$1/i;
  $group =~ s/\b(DENMARK|RAMADAN|ARAB)\b/\L\u$1/;
  $group =~ s/(Ramadan) (Series) (2024)/$1 $3 $2/;
  $group =~ s/(Syria)cs\b/$1/i;
  $group =~ s/Spanish\b/Spain/i;
  $group =~ s/^(.+)\s+(Movies|Series)\s*(.*)$/$2 $1 $3/i;
  $group =~ s/^(.*)(4K)(.*)$/$1 $3 $2/i;
  $group =~ s/\s{2,}/ /g;
  $group =~ s/EX YU:?/Ex-Yugoslavia/;
  $group =~ s/\btv\b/\Utv/g;
  $group =~ s/\bTranslated\b/translated/g;
  $group =~ s/\bTranslated\b/translated/g;
  $group =~ s/to (arabic)/to \u$1/;

  $title =~ s/^Adult //g;
  $title =~ s/^NFL\s+\|\s+\d+\s+- //g;

  # Adults is so.. top level
  $group =~ s/Adults/XXX/;

  $title =~ s/\\\'/\'/g;

  $data{title} = $title;
  $data{'tvg-name'} = $title;

  foreach (qw(group-title tvg-name title)) {
      $data{$_} =~ s/^\w+: //;
  }

  $group =~ s/^\s+//g;
  $group =~ s/\s+$//g;

  #$data{'tvg-country'} = country_to_code($group) unless $data{'tvg-country'};

  if ($group !~ /Server/) {
      $data{'group-title'} = $group;
      $section{$group} //= [];
      push(@{$section{$group}}, \%data);

      # Movies win
      $seen{ $data{uri} } = $group if $group eq 'Movies';
      $seen{ $data{uri} } = $group if $group eq 'Series';
      $seen{ $data{uri} } //= $group;
  }
  elsif (!$seen{$data{uri}}) {
      push(@nope, \%data);
  }
  $processed++;
}
close($fh);

foreach (@nope) {
    next if exists $seen{$_->{uri}};
    warn Dumper $_;
}


my $out;

sub _print_line {
    print $out $_,$/ foreach @_;
}

if (!$config->{playlist}{itemized}) {
    open $out, '>', $output;
    _print_line("#EXTM3U");
}
else {
    (undef, $output, undef) = splitpath($output);
}

my @tvg_tags = map { "tvg-$_" } qw(
    id
    name
    logo
    country
    language
    type
    url
    group
    epgid
    epgurl
    epgshift
    radio
    timeshift
    archive
    tvgplaylist
    aspect-ratio
    audio-track
    closed-captions
    closed-captions-type
    content-type
    copyright
    duration
);
unshift(@tvg_tags, 'group-title');


my $stays;

use Text::Unidecode;
foreach my $name (sort keys %section) {
    if ($config->{playlist}{itemized}) {
        close($out) if $out;
        my $filename = unidecode($name);
        $filename =~ s/[^[:word:]]+/_/g;

        open($out, '>', lc(catfile($output, "$filename.m3u")));
        _print_line("#EXTM3U");
    }

    my @list = sort { $a->{title} cmp $b->{title} } @{$section{$name}};

    my %seen_here;

    warn "Processing $name", $/;
    foreach my $d (@list) {
        next if $seen_here{$d->{uri}};
        $seen_here{$d->{uri}} = 1;
        #if ($name =~ /Movies|Series/ && $seen{$d->{uri}}) {
        #    next if $seen{$d->{uri}} eq 'Movies' && $name ne 'Movies';
        #    next if $seen{$d->{uri}} eq 'Series' && $name ne 'Series';
        #    next if $seen{$d->{uri}} eq 'Movies Drama' && $name ne 'Movies Drama';
        #}
        #if ($seen{$d->{uri}} ne $name) {
        #    warn "$d->{title} already found in $seen{$d->{uri}}", $/;
        #}
        my @line = ("#EXTINF:$d->{runtime}");

        foreach (@tvg_tags) {
            next unless defined $d->{$_};
            next unless length $d->{$_};
            push(@line, join('=', $_, "\"$d->{$_}\""));
        }

        my $line = join(' ', @line) . ",$d->{title}";
        _print_line(join("\n", $line, $d->{uri}));;
        $stays++;
    }
}

warn "Processed $processed stations to $stays stations", $/;
