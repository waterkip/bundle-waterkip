use strict;
use warnings;
use Test::More 0.96;

use Bundle::WATERKIP::Elfproef;
use Data::Random::NL qw(generate_bsn);


my $bsn = generate_bsn();
ok(elfproef($bsn, 1), "BSN is checked correctly");

done_testing;
