#
# This is a skeleton Dockerfile.
# It is not intended to be small or super nifty, it tries to cache some,
# but it is intended to be easy to go into an environment and poke
# around and edit and less things
#
FROM registry.gitlab.com/opndev/perl5/docker-p5/fat:latest

RUN <<OEF
apt-get update
apt-get install -y --no-install-recommends \
    desktop-file-utils libmagic-dev \
    libssl-dev libssl3 openssl make shared-mime-info zlib1g-dev
OEF

COPY cpanfile .

# HTTP::Daemon::SSL has several bugs open
# https://rt.cpan.org/Public/Bug/Display.html?id=146478
# https://rt.cpan.org/Public/Bug/Display.html?id=88998
# https://rt.cpan.org/Public/Bug/Display.html?id=81932
RUN <<OEF
docker-cpanm --notest HTTP::Daemon::SSL
docker-cpanm --notest Test::Vars
docker-cpanm --installdeps --test-only .
docker-cpanm -n --installdeps .
OEF

COPY . .
RUN prove -l && docker-cpanm -n .
