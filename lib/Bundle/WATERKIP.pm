use utf8;
package Bundle::WATERKIP;
our $VERSION = '0.004';

# ABSTRACT: A mono repo for perl scripts and modules which WATERKIP likes

1;

__END__

=head1 DESCRIPTION

This module serves a simple purpose. It is the main repository for all Perl
related things WATERKIP has come to rely on. It installs modules that I want
for development purposes. And it ships the bin scripts I want to have on my
machines.

=head1 SYNOPSIS

    cpanm Bundle::WATERKIP

=head1 SEE ALSO

If you want to know what is installed, see the dependencies :)
