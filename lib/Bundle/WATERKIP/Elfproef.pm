package Bundle::WATERKIP::Elfproef;
use warnings;
use strict;

use Exporter qw(import);

our @EXPORT = qw(elfproef);

=head2 elfproef

Check if the value passes the elf-proef.
If you supply an extra boolean, you will test if a value
passes the BSN elf-proef.

=cut

sub elfproef {
    my $number = shift;
    my $is_bsn = shift;

    my $l = length($number);
    if ($l > 6 && $l < 10) {
        $number = sprintf("%09d", $number);
    }
    else {
        die("'$number' isn't 9 characters long");
    }

    my $sum = 0;
    foreach (reverse(-9 .. -1)) {
        if ($is_bsn && $_ == -1) {
            $sum += (substr($number, $_, 1) * $_);
        }
        else {
            $sum += (substr($number, $_, 1) * abs($_));
        }
    }

    return $sum % 11 ? 0 : 1;
}

1;
